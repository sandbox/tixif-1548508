<?php
/**
 * @file
 * Formats the link to the article.  Takes several parameters into account.
 *
 * @param $data
 *   $data is an associative array of extra data regarding the record.
 *   $data['new-window'] => make the link with target="_blank".
 *   $data['text'] => the text for the link.
 *   $data['link'] => the url for the link.
 *   $data['fulltext'] => Does the record claim to be be full text?
 */
?>
<div class="article-discovery-link">
  <div class="article-discovery-link-inner">
<?php
  /*print l(
    $data['text'],
    $data['link'],
    array(
      'html' => TRUE,
      'attributes' => $data['attributes'],
    )
  );*/
?>
<?php
  if ($data['fulltext']) {
    $text = 'Full Text Online';
  }
  else {
      $text = 'Citation Online (no full text online)';
  }
  
  if ($data['source_type'] == 'Library Catalog') {
	$text = 'Localisation & disponibilité';
  }
?>
      <?php print l(
        t("$text"), 
        $data['link'], 
        array(
          'html' => TRUE,
          'attributes' => $data['attributes'],
        )
      );?>
  </div>
</div>
