<?php
/**
 * @file
 * Handles the rendering of a "Back to search results"  type of link.
 *
 * @param $data
 *   Associative array: $data['search']['uri']  provides the root uri,
 *   $data['search']['query']  provides the variables for the query string
 */

?>
<div class="article-discovery-details-navigation">
<?php if (
  isset($data) &&
  isset($data['uri']) &&
  isset($data['query'])
) : ?>
  <div><a href="<?php print $data['uri'] . '?' . http_build_query($data['query']); ?> "><?php print t('Back to article discovery results'); ?><span></a></div>
<?php endif; ?>
</div>

